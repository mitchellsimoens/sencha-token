'use strict';

module.exports = {
    get Manager () {
        return require('./Manager');
    },

    api : {
        get Key () {
            return require('./api/Key');
        },

        get Token () {
            return require('./api/Token');
        }
    }
};
